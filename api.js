const fs = require('fs');
const https = require('https');

// 获取用户输入的目标URL作为参数
let targetUrl = process.argv[2]; // 第三个参数是用户输入的URL

if (!targetUrl) {
  console.log('Please provide a target URL as an argument.');
  process.exit(1);
}

// 将输入的URL从 "https://kemono.su/patreon/user/690415" 转换为 "https://kemono.su/api/v1/patreon/user/690415"
targetUrl = targetUrl.replace('/patreon', '/api/v1/patreon');

// 创建一个正则表达式来匹配类似 "o=0" 或 "o=50" 的字符串
const regex = /o=(\d+)/;

// 使用正则表达式的 test 方法检查字符串中是否存在匹配
if (!regex.test(targetUrl)) {
  // 如果没有匹配，将 "o=0" 追加到字符串的末尾
  targetUrl += (targetUrl.includes('?') ? '&' : '?') + "o=0";
}
console.log(targetUrl);

// 从URL中提取一串数字，用于保存JSON文件
const userId = targetUrl.match(/\/user\/(\d+)/)[1];

// 保存所有JSON数据的数组
const allData = [];

// 设置请求头部，模拟 Chrome 浏览器的请求
const headers = {
  'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.0.0 Safari/537.36',
  'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
  'Accept-Language': 'en-US,en;q=0.9',
  'cookie':"session=eyJfcGVybWFuZW50Ijp0cnVlLCJhY2NvdW50X2lkIjozNDkwNjB9.ZUnIZA.71QIN3__GxzeWtD7vGuz1Lp7MD4",
  
};

// 递归函数来获取不同页的数据
function fetchData(url) {
  const options = {
    headers: headers,
  };

  https.get(url, options, (res) => {
    let data = '';

    res.on('data', (chunk) => {
      data += chunk;
    });

    res.on('end', () => {
      // 解析JSON数据
      const jsonData = JSON.parse(data);

      if (jsonData.length > 0) {
        // 合并数据
        allData.push(...jsonData);

        // 下一页的参数，每次增加50
        const nextPage = parseInt(url.match(/o=(\d+)/)[1]) + 50;

        // 构建下一页的URL
        const nextUrl = url.replace(url.match(/o=(\d+)/)[0], `o=${nextPage}`);
        console.log(nextUrl);

        // 继续请求下一页的数据
        fetchData(nextUrl);
      } else {
        // 所有数据已获取完毕
        saveDataToFile(userId);
      }
    });
  });
}

// 保存数据到文件，文件名使用用户ID
function saveDataToFile(userId) {
  const combinedData = JSON.stringify(allData);
  const filename = `jsons/user_${userId}.json`;
  fs.writeFileSync(filename, combinedData);
  console.log(`Data saved to ${filename}`);
}

// 开始获取数据
fetchData(targetUrl);
