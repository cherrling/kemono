import os
import re
import json
import requests

# 获取用户输入的目标 URL 作为参数
import sys

target_url = sys.argv[1] if len(sys.argv) > 1 else None

if not target_url:
    print('Please provide a target URL as an argument.')
    sys.exit(1)

# 将输入的 URL 从 "https://kemono.su/patreon/user/690415" 转换为 "https://kemono.su/api/v1/patreon/user/690415"
target_url = target_url.replace('/patreon', '/api/v1/patreon')

# 创建一个正则表达式来匹配类似 "o=0" 或 "o=50" 的字符串
regex = r'o=(\d+)'

# 使用正则表达式的 search 方法检查字符串中是否存在匹配
match = re.search(regex, target_url)

if not match:
    # 如果没有匹配，将 "o=0" 追加到字符串的末尾
    target_url += ('&' if '?' in target_url else '?') + 'o=0'

print(target_url)

# 从 URL 中提取一串数字，用于保存 JSON 文件
user_id = re.search(r'/user/(\d+)', target_url).group(1)

# 保存所有 JSON 数据的列表
all_data = []

# 设置请求头部，模拟 Chrome 浏览器的请求
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.0.0 Safari/537.36',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.9',
    'cookie': "session=eyJfcGVybWFuZW50Ijp0cnVlLCJhY2NvdW50X2lkIjozNDkwNjB9.ZUnIZA.71QIN3__GxzeWtD7vGuz1Lp7MD4",
}

# 递归函数来获取不同页的数据
def fetch_data(url):
    response = requests.get(url, headers=headers,timeout=100)
    data = response.text

    # 解析 JSON 数据
    json_data = json.loads(data)

    if len(json_data) > 0:
        # 合并数据
        all_data.extend(json_data)

        # 下一页的参数，每次增加 50
        next_page = int(re.search(r'o=(\d+)', url).group(1)) + 50

        # 构建下一页的 URL
        next_url = re.sub(r'o=\d+', f'o={next_page}', url)
        print(next_url)

        # 继续请求下一页的数据
        fetch_data(next_url)
    else:
        # 所有数据已获取完毕
        save_data_to_file(user_id)

# 保存数据到文件，文件名使用用户 ID
def save_data_to_file(user_id):
    combined_data = json.dumps(all_data)
    filename = f'jsons/user_{user_id}.json'
    with open(filename, 'w') as file:
        file.write(combined_data)
    print(f'Data saved to {filename}')

# 开始获取数据
fetch_data(target_url)
