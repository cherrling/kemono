const fs = require('fs');
const http = require('http');
const https = require('https');

function handleRedirects(url) {
    const protocol = url.startsWith('https://') ? https : http;

    const request = protocol.get(url, response => {
        if (response.statusCode >300&&response.statusCode<400) {
            // 如果是重定向，继续处理
            const redirectedUrl = response.headers.location;
            console.log('Redirected URL:', redirectedUrl);
            // 递归调用处理下一个重定向
            handleRedirects(redirectedUrl);
        } else {
            // 不再有重定向，输出最终的链接
            console.log('Final URL:', url);
        }
    });

    request.on('error', error => {
        console.error('HTTP Request Error:', error);
    });
}

// 读取本地JSON文件
fs.readFile('input.json', 'utf8', (err, data) => {
    if (err) {
        console.error('Error reading JSON file:', err);
        return;
    }

    try {
        const jsonData = JSON.parse(data);

        // 处理每个JSON对象
        jsonData.forEach(item => {
            // 添加"http://"或"https://"前缀
            const url = 'http://kemono.su' + item.file.path;
            // 开始处理重定向
            handleRedirects(url);
        });
    } catch (e) {
        console.error('Error parsing JSON data')
    }
})
