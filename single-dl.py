import requests

# url = "http://kemono.su/4c/a8/4ca81882000ff2927a57aea88fc18b8b79e3dd5e5025c7b3dcf1356545e0ffc3.jpg"

# 自定义头部信息
headers = {
      'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.0.0 Safari/537.36',
  'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
  'Accept-Language': 'en-US,en;q=0.9',
  'cookie':"session=eyJfcGVybWFuZW50Ijp0cnVlLCJhY2NvdW50X2lkIjozNDkwNjB9.ZUnIZA.71QIN3__GxzeWtD7vGuz1Lp7MD4",
}

proxy = {
    "http": "http://127.0.0.1:10809",
    "https": "http://127.0.0.1:10809"
}
# 设置最大重定向次数
# max_redirects = 5  # 你可以根据需要更改这个值

# # 使用Session对象来处理可能的多次重定向，并设置最大重定向次数
# with requests.Session() as session:
#     response = session.get(url, allow_redirects=True,headers=headers,proxies=proxy)
    
#     if response.status_code == 200:
#         # with open("downloaded_image.jpg", "wb") as f:
#         #     f.write(response.content)
#         # print("图像已成功下载")
#         print(response.url)
#     else:
#         print("下载失败，状态码:", response.status_code)
        


import json

# 读取 input.json 文件
with open('input.json', 'r') as file:
    data = json.load(file)

def CHANGEURL(path):
    # 在路径前面添加 "https://kemono.su"
    url="https://kemono.su"+path
    print(url)
    with requests.Session() as session:
        response = session.get(url, allow_redirects=True,headers=headers,proxies=proxy)
        
        if response.status_code == 200:
            with open(url+"downloaded_image.jpg", "wb") as f:
                f.write(response.content)
            print("图像已成功下载")
            print(response.url)
            return response.url

        else:
            return -1
        


# 对每个项目中的file和attachments的path进行处理
for item in data:
    while(1):
        x=CHANGEURL(item['file']['path'])
        if(x==-1):
            continue
        else:
            item['file']['path'] = x
            print(x)
            break

    for attachment in item['attachments']:
        while(1):
            x=CHANGEURL(attachment['path'])
            if(x==-1):
                continue

            else:
                attachment['path'] =x
                print(x)

                break

# 将处理后的数据保存为另一个 JSON 文件
with open('output.json', 'w') as file:
    json.dump(data, file, indent=2)
