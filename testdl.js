const http = require('http');
const https = require('https');
const fs = require('fs');
const url = require('url');
const { log } = require('console');
const headers = {
  'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.0.0 Safari/537.36',
  'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
  'Accept-Language': 'en-US,en;q=0.9',
  'cookie':"session=eyJfcGVybWFuZW50Ijp0cnVlLCJhY2NvdW50X2lkIjozNDkwNjB9.ZUnIZA.71QIN3__GxzeWtD7vGuz1Lp7MD4",
  
};

const downloadFile = (fileUrl, outputPath, maxRedirects) => {
  const parsedUrl = new URL(fileUrl);
  const protocol = parsedUrl.protocol === 'https:' ? https : http;

  const options = {
    hostname: parsedUrl.hostname,
    path: parsedUrl.pathname,
    port: parsedUrl.protocol === 'https:' ? 443 : 80,
    headers:headers,
    // maxRedirects: maxRedirects || 5,
    maxRedirects: 5,
  };

  const request = protocol.get(options, (response) => {
    console.log(response.statusCode);
    if (response.statusCode === 200) {
      console.log(response);
      // const fileStream = fs.createWriteStream(outputPath);
      // response.pipe(fileStream);
      // fileStream.on('finish', () => {
      //   fileStream.close();
      //   console.log('File downloaded successfully.');
      // });
    } else if (response.statusCode >= 300 && response.statusCode < 400 && response.headers.location) {
      console.log('Following redirect to: ', response.headers.location);
      downloadFile(response.headers.location, outputPath, maxRedirects - 1);

    } else {
      console.error('Failed to download the file. HTTP status code: ', response.statusCode);
    }
  });

  request.on('error', (error) => {
    console.error('Download error:', error);
  });
};

const fileUrl = 'http://kemono.su/4c/a8/4ca81882000ff2927a57aea88fc18b8b79e3dd5e5025c7b3dcf1356545e0ffc3.jpg';
const outputFilePath = 'downloaded_image.jpg';

downloadFile(fileUrl, outputFilePath);
